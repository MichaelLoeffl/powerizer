#ifndef POWER_SYSTEM_H
#define POWER_SYSTEM_H

#include <Arduino.h>
#include <EEPROM.h>

class BaseApp;

class PowerSystem
{
private:
  BaseApp* app_;

  float adcOffset_;
  float adcAmplification_;
  unsigned short eepromPosition_;
  unsigned short appOffset_;
  
  void ProcessButton(byte index, int state);
  void ProcessLedsAndButtons(unsigned short brightnessCompare);
  void ProcessLongPress();
public:
  PowerSystem();
  void Initialize();

  // Leds
  void SetFadeTime(unsigned short ms);
  void SetBrightness(byte led, byte brightness);
  void SetBrightnessIfHigher(byte led, byte brightness);

  // Trigger
  void Trigger(unsigned long ms);
  void TriggerOn(bool activate);
  bool GetTrigger();

  // DAC    
  void SetOutput(unsigned short value);
  void SetOutputNote(byte note);
  unsigned short GetOutput();

  // ADC
  unsigned short GetInput();
  byte GetInputNote();
  void SetAdcCorrection(float offset, float amplification);

  // App management
  byte GetSelectedApp();
  void SetApp(byte index, BaseApp* app);
  void SwitchApp(byte index);
  BaseApp* GetApp(byte index);
  byte GetPreviousApp();

  // EEPROM
  void SetPosition(unsigned short offset);
  void ResetPosition() {SetPosition(0);}

  template <typename T> void Write(unsigned short offset, T value)
  {
    EEPROM.put(offset + appOffset_, value);
  }

  template <typename T> void Read(unsigned short offset, T& out)
  {
    EEPROM.get(offset + appOffset_, out);
  }

  template <typename T> void Write(T value)
  {
    EEPROM.put(eepromPosition_, value);
    eepromPosition_ += sizeof(value);
  }

  template <typename T> void Read(T& out)
  {
    EEPROM.get(eepromPosition_, out);
    eepromPosition_ += sizeof(out);
  }

  // Main loop update
  void Update();
};

#endif // POWER_SYSTEM_H
