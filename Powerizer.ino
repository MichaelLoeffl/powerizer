#include "Quantizer.h"
#include "Menu.h"
#include "Keyboard.h"
#include "VoltageMeter.h"
#include "Calibrator.h"

Quantizer quantizer_;
Keyboard keyboard_;
VoltageMeter voltageMeter_;
Calibrator calibrator_;
Menu menu_;

void setup()
{
  Powerer.SetApp(0, &quantizer_);
  Powerer.SetApp(3, &keyboard_);
  Powerer.SetApp(10, &calibrator_);
  Powerer.SetApp(11, &voltageMeter_);
  Powerer.SetApp(12, &menu_);

  Powerer.Initialize();
}

void loop() 
{ 
  Powerer.Update();
}
