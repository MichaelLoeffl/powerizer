#ifndef VOLTAGE_METER_H
#define VOLTAGE_METER_H

#include "BaseApp.h"

class VoltageMeter : public BaseApp
{
  private:
  public:
    void ButtonClick(byte button) override
    {
      Powerer.SetOutputNote((11 - button) * 10);
    }
    
    void Update(unsigned long deltaT) override
    {
      unsigned long cv = Powerer.GetInputNote() / 10;

      if (cv > 11) 
      {
        cv = 11;
      }
      Powerer.SetBrightness(11 - cv, 16);

      for (int i = 12 - cv; i < 12; i++)
      {
        Powerer.SetBrightnessIfHigher(i, 5);
      }

      /*cv = Powerer.GetInput();
      for (int i = 0; i < 12; i++)
      {
        Powerer.SetBrightness(i, (cv & (1 << i)) ? 16 : 0);
      }*/
    }
};

#endif // VOLTAGE_METER_H
