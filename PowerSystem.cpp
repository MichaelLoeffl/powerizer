#include "PowerSystem.h"
#include "BaseApp.h"
#include "SPI.h"

//LED Pins
const byte LR_0 = A3;
const byte LR_1 = A4;
const byte LR_2 = A5;
const byte LR_3 = 7;
const byte LC_0 = A0;
const byte LC_1 = A1;
const byte LC_2 = A2;
const byte trig = 8;

//Button Pins
const byte BC_0 = 2;
const byte BC_1 = 1;
const byte BC_2 = 0;
const byte BR_0 = 3;
const byte BR_1 = 4;
const byte BR_2 = 5;
const byte BR_3 = 6;

//SPI Bus Pins
const byte csADC = 9;
const byte csDAC = 10;

//Button states
const unsigned short longPressTime_ = 500;
static unsigned short buttonDownTime_;
static unsigned short enableLongPress_;
static unsigned short debounce[12];
static unsigned short buttonStates_ = 0x000;

//LED states
static unsigned short brightness_[12];

static unsigned short fadeTime_;
static unsigned long triggerTime_;
static bool triggerOn_;

static unsigned short cvIn_;
static byte inputNote_;
static unsigned short cvOut_;

//Device states
static unsigned long deviceTime_;
static unsigned long deltaT_;
static BaseApp noApp_;

static BaseApp* apps_[13];
static unsigned short eepromOffset_[13];
static byte selectedApp_;
static byte prevSelectedApp_;;

void dacWrite(unsigned short value);
unsigned short adcRead(byte channel);

PowerSystem Powerer;

void PowerSystem::Trigger(unsigned long ms)
{
  triggerTime_ = ms;
  triggerOn_ = triggerTime_ != 0;
}

void PowerSystem::TriggerOn(bool activate)
{
  triggerTime_ = 0;
  triggerOn_ = activate;
}

bool PowerSystem::GetTrigger()
{
  return triggerOn_;
}

byte PowerSystem::GetSelectedApp()
{
  return selectedApp_;
}

byte PowerSystem::GetPreviousApp()
{
  return prevSelectedApp_;
}

void PowerSystem::SetPosition(unsigned short offset)
{
  eepromPosition_ = eepromOffset_[selectedApp_] + offset;
}

void PowerSystem::SetAdcCorrection(float offset, float amplification)
{
  float off = offset * 408.0f;
  adcOffset_ = off;
  adcAmplification_ = amplification;
}

void PowerSystem::SetApp(byte index, BaseApp* app)
{
  apps_[index] = app;
}

void PowerSystem::SwitchApp(byte index)
{
  if ((index < 13) && (apps_[index] != nullptr))
  {
    if (EEPROM[0] != index)
    {
      EEPROM.put(0, index);
    }

    prevSelectedApp_ = selectedApp_;
    selectedApp_ = index;
    app_ = apps_[index];
    
    ResetPosition();
    appOffset_ = eepromOffset_[index];
    app_->Activate();
  }
}

BaseApp* PowerSystem::GetApp(byte index)
{
  if (index < 13)
  {
    return apps_[index];
  }
}

void PowerSystem::SetOutput(unsigned short value)
{
  cvOut_ = value;
}

unsigned short PowerSystem::GetOutput()
{
  return cvOut_;
}

// 34 per note ~ 3ct per lsb ~ 10 octaves = 4080
void PowerSystem::SetOutputNote(byte note)
{
  cvOut_ = (unsigned short)note * 34;
}

byte PowerSystem::GetInputNote()
{
  return inputNote_;
}

unsigned short PowerSystem::GetInput()
{
  return cvIn_;
}

void PowerSystem::SetFadeTime(unsigned short ms)
{
  fadeTime_ = ms >> 4;
}

void PowerSystem::SetBrightness(byte led, byte brightness)
{
  brightness_[led] = brightness * fadeTime_;
}

void PowerSystem::SetBrightnessIfHigher(byte led, byte brightness)
{
  unsigned short temp = brightness * fadeTime_;
  if (temp > brightness_[led])
  {
    brightness_[led] = temp;
  }
}

PowerSystem::PowerSystem() 
{
  adcOffset_ = 0.0f;
  adcAmplification_ = 1.0f;
  deviceTime_ = 0;
  SetFadeTime(500);
  app_ = &noApp_;

  for (int i = 0; i < 13; i++)
  {
    apps_[i] = nullptr;
  }
  
  //Setup LED Pins
  pinMode(LR_0, OUTPUT);
  pinMode(LR_1, OUTPUT);
  pinMode(LR_2, OUTPUT);
  pinMode(LR_3, OUTPUT);
  pinMode(LC_0, OUTPUT);
  pinMode(LC_1, OUTPUT);
  pinMode(LC_2, OUTPUT);

  digitalWrite(LR_0, HIGH);
  digitalWrite(LR_1, HIGH);
  digitalWrite(LR_2, HIGH);
  digitalWrite(LR_3, HIGH);
  digitalWrite(LC_0, LOW);
  digitalWrite(LC_1, LOW);
  digitalWrite(LC_2, LOW);

  //Setup Button Pins
  pinMode(BC_0, OUTPUT);
  pinMode(BC_1, OUTPUT);
  pinMode(BC_2, OUTPUT);
  pinMode(BR_0, INPUT);
  pinMode(BR_1, INPUT);
  pinMode(BR_2, INPUT);
  pinMode(BR_3, INPUT);

  digitalWrite(BC_0, LOW);
  digitalWrite(BC_1, LOW);
  digitalWrite(BC_2, LOW);

  //Setup SPI Chip Select Pins
  pinMode(csADC, OUTPUT);
  pinMode(csDAC, OUTPUT);

  digitalWrite(csADC, HIGH);
  digitalWrite(csDAC, HIGH);
  SPI.begin();

  //Setup Trigger Pin
  pinMode(trig, OUTPUT);
  
  digitalWrite(trig, HIGH);
}

void PowerSystem::Initialize()
{
  eepromOffset_[0] = 10;

  for (int i = 0; i < 12; i++)
  {
    if (apps_[i] != nullptr)
    {
      selectedApp_ = i;
      apps_[i]->Initialize();
      eepromOffset_[i + 1] = eepromOffset_[i] + apps_[i]->GetRequiredSpace();
    }
    else
    {
      eepromOffset_[i + 1] = eepromOffset_[i];
    }
  }
  
  selectedApp_ = 12;
  apps_[12]->Initialize();

  selectedApp_ = EEPROM[0];
  if (selectedApp_ >= 13 || apps_[selectedApp_] == nullptr)
  {
    selectedApp_ = 12;
  }

  SwitchApp(selectedApp_);
}

void PowerSystem::Update()
{
  static byte softPwm = 0;
  
  softPwm++;
  if (softPwm == 16)
  {
    softPwm = 0;
  }
  
  unsigned long temp = millis();
  deltaT_ = temp - deviceTime_;
  deviceTime_ = temp;

  ProcessLedsAndButtons(softPwm * fadeTime_);
  ProcessLongPress();

  app_->Update(deltaT_);

  dacWrite(cvOut_);
  cvIn_ = adcRead(1);

  float adc = cvIn_ * adcAmplification_ + adcOffset_;
  if (adc < 0.0f)
  {
    adc = 0.0f;
  }
  else if (adc > 4080.0f)
  {
    adc = 4080.0f;
  }
  cvIn_ = (unsigned short)(adc + 0.5f);

  // ADC note hysteresis
  unsigned short diff = abs((unsigned short)inputNote_ * 34 - (cvIn_ + 17));
  if (diff > 25)
  {
    inputNote_ = (cvIn_ + 17) / 34;
  }
  
  // Trigger
  digitalWrite(trig, triggerOn_);
  if (triggerTime_ != 0)
  {
    if (triggerTime_ <= deltaT_)
    {
      triggerTime_ = 0;
      triggerOn_ = false;
    }
    else
    {
      triggerTime_ -= deltaT_;
    }
  }
}

void PowerSystem::ProcessButton(byte index, int buttonState)
{
  // Fade LED
  if (brightness_[index] >= deltaT_)
  {
    brightness_[index] -= deltaT_;
  }
  else
  {
    brightness_[index] = 0;
  }

  unsigned short mask = 1 << index;
  unsigned short state = (buttonStates_ & mask);
  unsigned short stateIn = buttonState ? mask : 0;
  
  if (stateIn != state)
  {
    if (debounce[index] > 50)
    {
      buttonStates_ &= ~mask;
      buttonStates_ |= stateIn;
      debounce[index] = 0;

      if (stateIn)
      {
        app_->ButtonDown(index);
        buttonDownTime_ = 0;
      }
      else
      {
        app_->ButtonUp(index);
        if (buttonDownTime_ < longPressTime_)
        {
          buttonDownTime_ = 0;
          app_->ButtonClick(index);
        }
      }
    }
    else
    {
      debounce[index] += deltaT_;
    }
  }
  else
  {
    debounce[index] = 0;
  }
}

void PowerSystem::ProcessLongPress()
{
  if (buttonStates_)
  {
    buttonDownTime_ += deltaT_;
    if (buttonDownTime_ >= longPressTime_)
    {
      buttonDownTime_ = longPressTime_;
      if (enableLongPress_)
      {
        if (buttonStates_ == 0x801)
        {
          if (selectedApp_ == 12)
          {
            asm volatile ("jmp 0");
          }
          SwitchApp(12);
        }
        else
        {
          app_->ButtonsLongPress(buttonStates_);
        }
        enableLongPress_ = false;
      }
    }
  }
  else
  {
    enableLongPress_ = true;
  }
}

void PowerSystem::ProcessLedsAndButtons(unsigned short brightnessCompare)
{
  int note;
  // C
  digitalWrite(LC_2, brightness_[0] > brightnessCompare);
  digitalWrite(LR_0, LOW);
  
  digitalWrite(BC_2, HIGH);
  note = digitalRead(BR_0);
  digitalWrite(BC_2, LOW);
  ProcessButton(0, note);
  
  digitalWrite(LR_0, HIGH);
  digitalWrite(LC_2, LOW);
  
  // C#
  digitalWrite(LC_2, brightness_[1] > brightnessCompare);
  digitalWrite(LR_1, LOW);
  
  digitalWrite(BC_2, HIGH);
  note = digitalRead(BR_1);
  digitalWrite(BC_2, LOW);
  ProcessButton(1, note);
  
  digitalWrite(LR_1, HIGH);
  digitalWrite(LC_2, LOW);

  // D
  digitalWrite(LC_2, brightness_[2] > brightnessCompare);
  digitalWrite(LR_2, LOW);
  
  digitalWrite(BC_2, HIGH);
  note = digitalRead(BR_2);
  digitalWrite(BC_2, LOW);
  ProcessButton(2, note);
  
  digitalWrite(LR_2, HIGH);
  digitalWrite(LC_2, LOW);

  // D#
  digitalWrite(LC_2, brightness_[3] > brightnessCompare);
  digitalWrite(LR_3, LOW);
  
  digitalWrite(BC_2, HIGH);
  note = digitalRead(BR_3);
  digitalWrite(BC_2, LOW);
  ProcessButton(3, note);
  
  digitalWrite(LR_3, HIGH);
  digitalWrite(LC_2, LOW);

  // E
  digitalWrite(LC_1, brightness_[4] > brightnessCompare);
  digitalWrite(LR_0, LOW);
  
  digitalWrite(BC_1, HIGH);
  note = digitalRead(BR_0);
  digitalWrite(BC_1, LOW);
  ProcessButton(4, note);

  digitalWrite(LR_0, HIGH);
  digitalWrite(LC_1, LOW);

  // F
  digitalWrite(LC_1, brightness_[5] > brightnessCompare);
  digitalWrite(LR_1, LOW);
  
  digitalWrite(BC_1, HIGH);
  note = digitalRead(BR_1);
  digitalWrite(BC_1, LOW);
  ProcessButton(5, note);
  
  digitalWrite(LR_1, HIGH);
  digitalWrite(LC_1, LOW);

  // F#
  digitalWrite(LC_1, brightness_[6] > brightnessCompare);
  digitalWrite(LR_2, LOW);
  
  digitalWrite(BC_1, HIGH);
  note = digitalRead(BR_2);
  digitalWrite(BC_1, LOW);
  ProcessButton(6, note);
  
  digitalWrite(LR_2, HIGH);
  digitalWrite(LC_1, LOW);

  // G
  digitalWrite(LC_1, brightness_[7] > brightnessCompare);
  digitalWrite(LR_3, LOW);
  
  digitalWrite(BC_1, HIGH);
  note = digitalRead(BR_3);
  digitalWrite(BC_1, LOW);
  ProcessButton(7, note);
  
  digitalWrite(LR_3, HIGH);
  digitalWrite(LC_1, LOW);

  // G#
  digitalWrite(LC_0, brightness_[8] > brightnessCompare);
  digitalWrite(LR_0, LOW);
  
  digitalWrite(BC_0, HIGH);
  note = digitalRead(BR_0);
  digitalWrite(BC_0, LOW);
  ProcessButton(8, note);
  
  digitalWrite(LR_0, HIGH);
  digitalWrite(LC_0, LOW);

  // A
  digitalWrite(LC_0, brightness_[9] > brightnessCompare);
  digitalWrite(LR_1, LOW);
  
  digitalWrite(BC_0, HIGH);
  note = digitalRead(BR_1);
  digitalWrite(BC_0, LOW);
  ProcessButton(9, note);
  
  digitalWrite(LR_1, HIGH);
  digitalWrite(LC_0, LOW);

  // A#
  digitalWrite(LC_0, brightness_[10] > brightnessCompare);
  digitalWrite(LR_2, LOW);
  
  digitalWrite(BC_0, HIGH);
  note = digitalRead(BR_2);
  digitalWrite(BC_0, LOW);
  ProcessButton(10, note);
  
  digitalWrite(LR_2, HIGH);
  digitalWrite(LC_0, LOW);

  // H
  digitalWrite(LC_0, brightness_[11] > brightnessCompare);
  digitalWrite(LR_3, LOW);
  
  digitalWrite(BC_0, HIGH);
  note = digitalRead(BR_3);
  digitalWrite(BC_0, LOW);
  ProcessButton(11, note);
  
  digitalWrite(LR_3, HIGH);
  digitalWrite(LC_0, LOW);
}

void dacWrite(unsigned short value) 
{
  digitalWrite(csDAC, LOW);
  byte data = value >> 8;
  data = data & B00001111;
  data = data | B00110000;
  SPI.transfer(data);

  data = value & 0xFF;
  SPI.transfer(data);

  digitalWrite(csDAC, HIGH);
}

unsigned short adcRead(byte channel) 
{
  byte commandbits = B00001101;           //command bits - 0000, start, mode, chn, MSBF
  unsigned int b1 = 0;                    // get the return var's ready
  unsigned int b2 = 0;
  commandbits |= ((channel - 1) << 1);    // update the command bit to select either ch 1 or 2
  digitalWrite(csADC, LOW);
  SPI.transfer(commandbits);              // send out the command bits
  const int hi = SPI.transfer(b1);        // read back the result high byte
  const int lo = SPI.transfer(b2);        // then the low byte
  digitalWrite(csADC, HIGH);
  b1 = lo + (hi << 8);                    // assemble the two bytes into a word
  return (b1 >> 3);                       // We have got a 12bit answer but strip LSB's if
  // required >>4 ==10 bit (0->1024), >>2 ==12bit (0->4096)
}
