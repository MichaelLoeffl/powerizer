#ifndef CALIBRATOR_H
#define CALIBRATOR_H

#include "BaseApp.h"

#define MA_WINDOW_SIZE (32)

class Calibrator : public BaseApp
{
  private:
    byte volts_ = 0;
    unsigned short adcCalibration_ = 0;

    unsigned short movingAverage_[MA_WINDOW_SIZE];
    byte maIndex_;
    unsigned long maSum_;
    float filtered_;

    float offset_;
    float amplification_;

    float a_;
    float b_;

    void DoAutomaticCalibration()
    {
        if (adcCalibration_ > (MA_WINDOW_SIZE + 15))
        {
            volts_ = 6;
            b_ = filtered_ / 408.0f;
        }
        else
        {
            volts_ = 1;
            a_ = filtered_ / 408.0f;
        }

        adcCalibration_++;
        if (adcCalibration_ > (MA_WINDOW_SIZE + 15) * 2)
        {
            amplification_ = (b_ - a_) / 5.0f;
            amplification_ = 1.0f / amplification_;
            offset_ = 1.0f - a_;
            Powerer.ResetPosition();
            Powerer.Write(offset_);
            Powerer.Write(amplification_);
            Powerer.SetAdcCorrection(offset_, amplification_);
            
            adcCalibration_ = 0;
        }
    }

    void UpdateMovingAverage()
    {
        if (++maIndex_ >= MA_WINDOW_SIZE)
        {
            maIndex_ = 0;
        }
        
        maSum_ -= movingAverage_[maIndex_];
        unsigned short cv = Powerer.GetInput();
        movingAverage_[maIndex_] = cv;
        maSum_ += cv;
        filtered_ = maSum_ / (float)MA_WINDOW_SIZE;
    }
  public:
    void Activate() override
    {
        volts_ = 0;
        adcCalibration_ = 0;
        for (int i = 0; i < MA_WINDOW_SIZE; i++)
        {
            movingAverage_[i] = 0;
        }
        maSum_ = 0;
        maIndex_ = 0;

        Powerer.ResetPosition();
        Powerer.Read(offset_);
        Powerer.Read(amplification_);
        Powerer.SetAdcCorrection(offset_, amplification_);
    }

    void ButtonClick(byte button) override
    {
      if ((button & 1) == (button >= 5))
      {
        Powerer.SetBrightness(button, 16);
        volts_ = 6 - ((button + (button >= 5)) >> 1);
        adcCalibration_ = 0;
      }
      if (button == 8)
      {
        offset_ = 0.0f;
        amplification_ = 1.0f;
        Powerer.SetAdcCorrection(offset_, amplification_);
        adcCalibration_ = 1;
      }
      if (button == 1)
      {
        offset_ = 0.0f;
        amplification_ = 1.0f;
        Powerer.Write(offset_);
        Powerer.Write(amplification_);
        Powerer.SetAdcCorrection(offset_, amplification_);
        adcCalibration_ = 0;
      }
    }

    void Update(unsigned long deltaT) override
    {
        UpdateMovingAverage();

        if (adcCalibration_ != 0)
        {
            DoAutomaticCalibration();
        }

        if (volts_ <= 6)
        {
            byte button = (6 - volts_) * 2 - (volts_ < 4);
            Powerer.SetBrightness(button, 16);
        }
        
        Powerer.SetOutputNote(12 * volts_);

        long diff = ((long)Powerer.GetOutput() - (long)(filtered_ + 0.5f)) / 4;
        if (diff > 0)
        {
            if (diff > 16)
            {
                diff = 16;
            }
            Powerer.SetBrightness(10, diff);
        }
        else if (diff < 0)
        {
            diff = -diff;
            if (diff > 16)
            {
                diff = 16;
            }
            Powerer.SetBrightness(6, diff);
        }

        Powerer.SetBrightness(8, 16 - diff);
    }
};

#endif // CALIBRATOR_H
