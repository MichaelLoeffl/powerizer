#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "BaseApp.h"

const byte KeyboardRange = 5 * 12;

class Keyboard : public BaseApp
{
  private:
    byte offset_;
    byte mode_;
    byte currentNote_;
  public:
    void Initialize() override
    {
      Powerer.ResetPosition();
      Powerer.Read(offset_);
    }

    void Activate() override
    {
      mode_ = 0;
      currentNote_ = 12;
    }

    void ButtonsLongPress(unsigned short buttons) override
    {
      if (buttons == 0xC00)
      {
        mode_ = 1;
        Powerer.TriggerOn(false);
        currentNote_ = 12;
      }
    }

    void ButtonClick(byte button) override
    {
      switch (mode_)
      {
      case 1:
        if (button < 4)
        {
          Powerer.SetBrightness(button, 16);
          offset_ = button * 12 + offset_ % 12;
          mode_ = 2;
        }
        if (button == 11)
        {
          mode_ = 0;
        }
        break;
      case 2:
        Powerer.SetBrightness(button, 16);
        offset_ = button + offset_ / 12 * 12;
        mode_ = 0;

        Powerer.ResetPosition();
        Powerer.Write(offset_);
        break;
      }
    }

    void ButtonDown(byte button) override
    {
      if (mode_ == 0)
      {
        Powerer.SetBrightness(button, 16);
        Powerer.TriggerOn(true);
        Powerer.SetOutputNote(button + offset_);
        currentNote_ = button;
      }
    }

    void ButtonUp(byte button) override
    {
      if (currentNote_ == button)
      {
        Powerer.SetBrightness(button, 16);
        Powerer.TriggerOn(false);
        currentNote_ = 12;
      }
    }

    void Update(unsigned long deltaT) override
    {
      switch (mode_)
      {
        case 1:
        {
          Powerer.SetBrightnessIfHigher(0, 4);
          Powerer.SetBrightnessIfHigher(1, 4);
          Powerer.SetBrightnessIfHigher(2, 4);
          Powerer.SetBrightnessIfHigher(3, 4);
          Powerer.SetBrightnessIfHigher(offset_ / 12, 16);
        } break;
      case 2:
        {
          Powerer.SetBrightnessIfHigher(0, 4);
          Powerer.SetBrightnessIfHigher(1, 4);
          Powerer.SetBrightnessIfHigher(2, 4);
          Powerer.SetBrightnessIfHigher(3, 4);
          Powerer.SetBrightnessIfHigher(4, 4);
          Powerer.SetBrightnessIfHigher(5, 4);
          Powerer.SetBrightnessIfHigher(6, 4);
          Powerer.SetBrightnessIfHigher(7, 4);
          Powerer.SetBrightnessIfHigher(8, 4);
          Powerer.SetBrightnessIfHigher(9, 4);
          Powerer.SetBrightnessIfHigher(10, 4);
          Powerer.SetBrightnessIfHigher(11, 4);
          Powerer.SetBrightnessIfHigher(offset_ % 12, 16);
        } break;
      default:
        {
          if (currentNote_ < 12)
          {
            Powerer.SetBrightness(currentNote_, 16);
          }
        } break;
      }
      
    }
};

#endif // KEYBOARD_H
