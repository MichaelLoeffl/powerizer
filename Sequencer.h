#ifndef SEQUENCER_APP_H
#define SEQUENCER_APP_H

#include "BaseApp.h"

class Tourist : public BaseApp
{
private:
  byte note_[64];
  byte length_;
  bool active_[12];
public:
  void Activate() override
  {
    Powerer.TriggerOn(false);
  }

  void ButtonClick(byte button) override
  {
    active_[button] = !active_[button];
    Powerer.SetBrightness(button, 0);
  };
  
  void ButtonsLongPress(unsigned short buttons) override
  {
    switch (buttons)
    {
      case 0x0C00:
        brightness_ = (brightness_ == 16) ? 5 : 16;
        Powerer.Trigger(10);
        break;
    }
  };

  void Update(unsigned long deltaT) override
  {
    
  }
};

#endif // QUANTISER_APP_H
