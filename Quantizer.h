#ifndef QUANTISER_APP_H
#define QUANTISER_APP_H

#include "BaseApp.h"

class Quantizer : public BaseApp
{
private:
  byte brightness_ = 5;
  bool active_[12];
  byte lastNote_ = 13;
  byte delayNote_ = 13;
  unsigned long freeze_ = 0;
  unsigned long delay_ = 0;
  unsigned short memory_ = 0;
  unsigned short memoryPrev_ = 0;
public:
  void Initialize() override
  {
    Powerer.ResetPosition();
    Powerer.Read(memory_);
    memoryPrev_ = memory_;

    for (int i = 0; i < 12; i++)
    {
      active_[i] = ((memory_ & (1 << i)) != 0);
    }

    brightness_ = ((memory_ & 0x8000) != 0) ? 16 : 5;
  }

  void Activate() override
  {
    Powerer.TriggerOn(false);
    freeze_ = 10;
    delay_ = 0;
    delayNote_ = 13;
  }

  void ButtonClick(byte button) override
  {
    unsigned short mask = 1 << button;
    memory_ = memory_ ^ mask;

    active_[button] = ((memory_ & mask) != 0);
    Powerer.SetBrightness(button, 0);
  };
  
  void ButtonsLongPress(unsigned short buttons) override
  {
    switch (buttons)
    {
      case 0x0C00:
        memory_ = memory_ ^ 0x8000;
        brightness_ = ((memory_ & 0x8000) != 0) ? 16 : 8;
        Powerer.Trigger(10);
        break;
    }
  };

  void Update(unsigned long deltaT) override
  {
    if (memory_ != memoryPrev_)
    {
      memoryPrev_ = memory_;
      Powerer.ResetPosition();
      Powerer.Write(memory_);
    }
    byte note = Powerer.GetInputNote();
    
    // Store found note (if none is active, this initializes to current note)
    int minimum = 12;
    int fit = note;

    // Search range
    byte start;

    // Clip search range
    if      (note < 6)    start = 0;
    else if (note > 109)  start = 109;
    else                  start = note - 5;

    byte end = start + 12;

    // Search for active note
    for (byte i = start; i < end; i++)
    {
      byte index = i % 12;
      if (active_[index])
      {
        int dist = abs(i - note);
        if (dist < minimum)
        {
          minimum = dist;
          fit = i;
        }
        // Show active notes
        Powerer.SetBrightness(index, brightness_);
      }
    }
    note = fit;

    // Don't react to new notes until trigger is done (13ms)
    if (freeze_ > deltaT)
    {
      freeze_ -= deltaT;
    }
    else
    {
      freeze_ = 0;
      
      if (lastNote_ != note && delay_ == 0)
      {
        Powerer.SetOutputNote(note);
        Powerer.Trigger(10);
        freeze_ = 20;
        lastNote_ = note;
      }
      else if (delayNote_ == note)
      {
        if (delay_ > deltaT)
        {
          delay_ -= deltaT;
        }
        else
        {
          delay_ = 0;
        }
      }
      else 
      {
        delayNote_ = note;
        delay_ = 0;
      }
    }

    // Highlight quantized note
    Powerer.SetBrightness(lastNote_ % 12, 16);
  }
};

#endif // QUANTISER_APP_H
