#ifndef BASE_APP_H
#define BASE_APP_H

#include "PowerSystem.h"

extern PowerSystem Powerer;

class BaseApp
{
public:
  virtual void Initialize() {};
  virtual void Activate() {};

  virtual unsigned short GetRequiredSpace() { return 0; };

  virtual void ButtonDown(byte button) {};
  virtual void ButtonUp(byte button) {};
  virtual void ButtonClick(byte button) {};
  virtual void ButtonsLongPress(unsigned short buttons) {};

  virtual void Update(unsigned long deltaT) {};
};

#endif // BASE_APP_H
