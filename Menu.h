#ifndef MENU_H
#define MENU_H

#include "BaseApp.h"

class Menu : public BaseApp
{
  private:
    unsigned long time_;
    unsigned int appStack_;
  public:
    void Activate() override
    {
      Powerer.TriggerOn(false);
      time_ = 0;
      appStack_ = 0;
      for (int i = 0; i < 12; i++)
      {
        appStack_ |= ((Powerer.GetApp(i) != nullptr) ? 1 : 0) << i;
      }
    }

    void ButtonClick(byte button) override
    {
      Powerer.SwitchApp(button);
    }

    void Update(unsigned long deltaT) override
    {
      // Update time
      time_ += deltaT;
      if (time_ > 10000)
      {
        time_ = 5000;
      }

      int max;
      // First flash and build up
      if (time_ < 5000)
      {
        max = time_ / 50;
        if (max < 12)
        {
          Powerer.SetBrightness(max, 16);
        }
      }
      // Mega cool effect flash every 5 seconds cause we can
      else
      {
        max = (time_ - 5000) / 50;
        if (max < 12)
        {
          Powerer.SetBrightnessIfHigher(max, Powerer.GetApp(max) == nullptr ? 8 : 16);
        }
        max = 12;
      }

      unsigned short stack = appStack_;
      byte currentApp = Powerer.GetPreviousApp();

      for (byte i = 0; (i < 12) && (i < max); i++)
      {
        if (stack & 1)
        {
          Powerer.SetBrightness(i, i == currentApp ? 16 : 8);
        }
        stack >>= 1;
      }
    }
};

#endif // MENU_H
